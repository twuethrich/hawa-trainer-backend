﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------



public partial class SelectDialogCatalog {
    
    /// <summary>
    /// form1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlForm form1;
    
    /// <summary>
    /// hf_picID-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hf_picID;
    
    /// <summary>
    /// SqlDataSource1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource1;
    
    /// <summary>
    /// SqlDataSource2-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource2;
    
    /// <summary>
    /// SqlDataSource3-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource3;
    
    /// <summary>
    /// SqlDataSource4-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource4;
    
    /// <summary>
    /// GridView1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView GridView1;
}
