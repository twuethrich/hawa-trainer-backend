﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Zusammenfassungsbeschreibung für ResizeImage
/// </summary>
public class ResizeImage
{
    /*
	public ResizeImage()
	{
		
       //This class resize the original image and save it into the right folder


	}*/
    public static void ImageSave(string Image1, string filename, int Size)
    {
        string FilePath = ConfigurationManager.AppSettings["ObjectPath"];
        //string Path = Image1;
        string Path = FilePath + Image1;
        Bitmap bmp = CreateThumbnail(Path, Size, Size);
       
        // Put user code to initialize the page here
        //Response.ContentType = "image/jpeg";
        bmp.Save(FilePath + filename, System.Drawing.Imaging.ImageFormat.Jpeg);
        bmp.Dispose();
        //return bmp;
    }

    public static void ImageCopy(string filename, int groupID, int Size)
    {
        string FilePath = ConfigurationManager.AppSettings["ObjectPath"];
        string FilePath2 = ConfigurationManager.AppSettings["ObjectPath2"];
        
        //string Path = Image1;
        //string Path = FilePath + Image1;
        string filePath = FilePath;

        switch (groupID)
        {
            case 37:
                filePath = FilePath2 + @"Katalogzeichnungen\thumbnails\" + filename + ".jpg"; break;
            case 48:
                filePath = FilePath2 + @"Isometrische_Darstellung_Artikel\thumbnails\" + filename + ".jpg"; break;
            case 29:
                filePath = FilePath2 + @"Systembild 1\" + filename + ".jpg"; break;
            case 30:
                filePath = FilePath2 + @"Systembild 2\" + filename + ".jpg"; break;
            case 31:
                filePath = FilePath2 + @"Systembild 3\" + filename + ".jpg"; break;
            case 99:
                filePath = FilePath2 + @"references\" + filename + ".jpg"; break;
            default:
                filePath = FilePath + filename + ".jpg"; break;
        }

        Bitmap bmp = CreateThumbnail(filePath, Size, Size);

        
        // Put user code to initialize the page here
        // Response.ContentType = "image/jpeg";
        bmp.Save(FilePath + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
        bmp.Dispose();

        //return bmp;
    }

    public static Bitmap CreateThumbnail(string lcFilename, int lnWidth, int lnHeight)
    {
        System.Drawing.Bitmap bmpOut = null;
        try
        {
            Bitmap loBMP = new Bitmap(lcFilename);
            ImageFormat loFormat = loBMP.RawFormat;
            decimal lnRatio;
            int lnNewWidth = 0;
            int lnNewHeight = 0;
            //*** If the image is smaller than a thumbnail just return it
            if (loBMP.Width < lnWidth && loBMP.Height < lnHeight)
                return loBMP;


            //if (loBMP.Width > loBMP.Height)
            //{
            lnRatio = (decimal)lnWidth / loBMP.Width;
            lnNewWidth = lnWidth;
            decimal lnTemp = loBMP.Height * lnRatio;
            lnNewHeight = (int)lnTemp;
            /*}
            else
            {
                lnRatio = (decimal)lnHeight / loBMP.Height;
                lnNewHeight = lnHeight;
                decimal lnTemp = loBMP.Width * lnRatio;
                lnNewWidth = (int)lnTemp;

            }*/
            // System.Drawing.Image imgOut = 
            //      loBMP.GetThumbnailImage(lnNewWidth,lnNewHeight,
            //                              null,IntPtr.Zero);
            // *** This code creates cleaner (though bigger) thumbnails and properly
            // *** and handles GIF files better by generating a white background for
            // *** transparent images (as opposed to black)
            bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
            Graphics g = Graphics.FromImage(bmpOut);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
            g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);
            loBMP.Dispose();

        }
        catch
        {
            return null;
        }

        return bmpOut;
    }

    
}
