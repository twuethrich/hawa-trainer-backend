﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------



public partial class AttributeDetail {
    
    /// <summary>
    /// form1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlForm form1;
    
    /// <summary>
    /// Image1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Image Image1;
    
    /// <summary>
    /// Label1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label1;
    
    /// <summary>
    /// Label2-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label2;
    
    /// <summary>
    /// Label3-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label3;
    
    /// <summary>
    /// FormView1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.FormView FormView1;
    
    /// <summary>
    /// SqlDataSource1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource1;
}
