﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ProductList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            int artID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ART_ID"));
            int holz = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Holz"));
            int metall = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Metall"));
            int glas = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Glas"));
            int hasConfig = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "hasConfig"));

            e.Row.Cells[4].Text = "<img src=APP_Images/bullet_round_red.gif Border=0>";
            e.Row.Cells[5].Text = "<img src=APP_Images/bullet_round_red.gif Border=0>";
            e.Row.Cells[6].Text = "<img src=APP_Images/bullet_round_red.gif Border=0>";
            e.Row.Cells[7].Text = "<img src=APP_Images/bullet_round_red.gif Border=0>";
            if(holz > 0)
                e.Row.Cells[5].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";

            if(glas > 0)
                e.Row.Cells[5].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";

            if(metall > 0)
                e.Row.Cells[6].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";

            if (hasConfig > 0)
                e.Row.Cells[7].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";

            string aName = DataBinder.Eval(e.Row.DataItem, "Product").ToString();
            string xLink = "ThemeList.aspx?artID=" + artID+"&aName="+aName;

            e.Row.Cells[1].Attributes.Add("onclick", "openWindow('" + xLink + "','ThemeList', 500,600);");
            e.Row.Cells[1].Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
            btnEdit.Attributes.Add("onclick", "openWindow('" + xLink + "','ThemeList', 500,600);return false");

        }
        
    }

    
}
