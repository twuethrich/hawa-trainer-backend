﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ReferenceTypeList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Add("ART_ID", Request.QueryString.Get("artID"));
        Session.Add("Theme", Request.QueryString.Get("thema"));
        Session.Add("THM_ID", "3");
        Label2.Text = Session["Theme"].ToString();
        Label1.Text = Session["Product"].ToString();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {

            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");

            int attID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ATT_ID"));
            int rbtID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "RBT_ID"));

           
            string xLink = "AttributeDetail.aspx?attID=" + attID + "&attribute=Anwendungsbereich&rbtID=" + rbtID;
            btnEdit.Attributes.Add("onclick", "openWindow('" + xLink + "','newAttribute', 500,650)");

        }
    }
}
