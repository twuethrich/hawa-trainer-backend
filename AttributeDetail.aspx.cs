﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class AttributeDetail : System.Web.UI.Page
{
    public string FilePath = ConfigurationManager.AppSettings["ObjectPath"];
    public string FilePath2 = ConfigurationManager.AppSettings["ObjectPath2"];
    public int AGR_ID;
    public int RBT_ID;
    public int ATT_ID =-1;
    public int ART_ID;
    public int THM_ID;
    public Image dispImage1;
    public Image dispImage2;
    public string filename = "";
    public string filename2 = "";
    public string langID = "DE";
  
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = Session["Product"].ToString();
        Label2.Text = Session["Theme"].ToString();
       
        
        if(Request.QueryString.Get("agrID") != null)
            AGR_ID = Convert.ToInt32(Request.QueryString.Get("agrID"));

        if (Request.QueryString.Get("rbtID") != null)
            RBT_ID = Convert.ToInt32(Request.QueryString.Get("rbtID"));

        if(Request.QueryString.Get("attID") != null)
            ATT_ID = Convert.ToInt32(Request.QueryString.Get("attID"));

        if (Request.QueryString.Get("artID") != null)
            ART_ID = Convert.ToInt32(Request.QueryString.Get("artID"));


        if (Request.QueryString.Get("thmID") != null)
            THM_ID = Convert.ToInt32(Request.QueryString.Get("thmID"));

        if (Request.QueryString.Get("langID") != null)
            langID = (String)Request.QueryString.Get("langID");
        
      

        if (ATT_ID == -1 && !Page.IsPostBack)
        {
            FormView1.DefaultMode = FormViewMode.Insert;
            Label3.Text = "Neu";
           
        }
        else
        {
            TextBox ATL_Name = (TextBox)FormView1.FindControl("ATL_Name");
            Label3.Text = ATL_Name.Text.ToString();
        }
        
       
    }
    protected void UpdateButton_Click(object sender, EventArgs e)
    {

        //Fileupload
        FileUpload file1 = (FileUpload)this.FormView1.FindControl("FileUpload1");
        FileUpload file2 = (FileUpload)this.FormView1.FindControl("FileUpload2");

        TextBox epsText = (TextBox)this.FormView1.FindControl("txtEPS1");
        TextBox epsText2 = (TextBox)this.FormView1.FindControl("txtEPS2");

        TextBox ATL_Name = (TextBox)this.FormView1.FindControl("ATL_Name");
        TextBox ATL_Name2 = (TextBox)this.FormView1.FindControl("ATL_Name2");
        TextBox ATL_Text1 = (TextBox)this.FormView1.FindControl("ATL_Text1");
        TextBox ATL_Text2 = (TextBox)this.FormView1.FindControl("ATL_Text2");

        HiddenField hdfGroupID1 = (HiddenField)this.FormView1.FindControl("GroupID");
        HiddenField hdfGroupID2 = (HiddenField)this.FormView1.FindControl("GroupID2");
        int groupID1 = Convert.ToInt32(hdfGroupID1.Value);
        int groupID2 = Convert.ToInt32(hdfGroupID2.Value);

        string aName = ATL_Name.Text.ToString();
        string aName2 = ATL_Name2.Text.ToString();
        string aText1 = ATL_Text1.Text.ToString();
        string aText2 = ATL_Text2.Text.ToString();

        if (file1.PostedFile.FileName != "")
        {
            filename = file1.PostedFile.FileName;
            if (file1.PostedFile.ContentLength > 10524288)
            {
                //Keine Dateien zulassen, die grösser wie 500KB sind
                filename = "";
                string scriptString1 = "<script language=JavaScript> " +
                "alert('Die Datei ist zu gross!');</script>";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString1))
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", scriptString1);
            }
            else
            {
                string[] xName = file1.PostedFile.FileName.ToString().Split('.');
                string extension = xName[1].ToString();
                //filename = ATT_ID + "_Image1." + extension.ToString();//
                filename = ATT_ID + "_1_" + Path.GetFileName(file1.PostedFile.FileName);
                string tempFile = "temp_" + Path.GetFileName(file1.PostedFile.FileName);
                file1.PostedFile.SaveAs(FilePath + tempFile);
                ResizeImage.ImageSave(tempFile, filename, 365);
                File.Delete(FilePath + tempFile);

            }
        }
        else
        {
            if (epsText.Text.Trim() != "")
            {
                filename = epsText.Text.ToString().Trim() + ".jpg";               
                string filenameCopy = epsText.Text.ToString().Trim();
                ResizeImage.ImageCopy(filenameCopy, groupID1, 365);

            }


        }

        if (file2.PostedFile.FileName != "")
        {
            filename2 = file2.PostedFile.FileName;
            if (file2.PostedFile.ContentLength > 10524288)
            {
                //Keine Dateien zulassen, die grösser wie 500KB sind
                filename = "";
                string scriptString2 = "<script language=JavaScript> " + "alert('Die Datei ist zu gross!');</script>";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString2))
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", scriptString2);
            }
            else
            {
                string[] xName2 = file2.PostedFile.FileName.ToString().Split('.');
                string extension2 = xName2[1].ToString();
                //filename2 = ATT_ID + "_Image2." + extension2.ToString();
                filename2 = ATT_ID + "_2_" + Path.GetFileName(file2.PostedFile.FileName);
                string tempFile2 = "temp_" + Path.GetFileName(file2.PostedFile.FileName);
                file2.PostedFile.SaveAs(FilePath + tempFile2);
                ResizeImage.ImageSave(tempFile2, filename2, 365);
                File.Delete(FilePath + tempFile2);

                //file2.PostedFile.SaveAs(FilePath + filename2);
                //ResizeImage.ImageSave(file2.PostedFile.FileName, filename2, 365);
            }
        }
        else
        {
            if (epsText2.Text.Trim() != "" && ATT_ID != 7)
            {
                filename2 = epsText2.Text.ToString().Trim() + ".jpg";       
                string filenameCopy2 = epsText2.Text.ToString().Trim();
                ResizeImage.ImageCopy(filenameCopy2, groupID2, 365);
            }

        }


        DataAccess.updateAttribute(ATT_ID,ART_ID,THM_ID,aName,aName2, filename,filename2, aText1,aText2,groupID1,groupID2,langID);
        
        
        string scriptString = "<script language=JavaScript> " + "window.opener.document.forms[0].submit();window.close();</script>";
        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"script", scriptString);

    }
    protected void InsertButton_Click(object sender, EventArgs e)
    {
        //Fileupload
        FileUpload file1 = (FileUpload)this.FormView1.FindControl("FileUpload1");
        FileUpload file2 = (FileUpload)this.FormView1.FindControl("FileUpload2");

        TextBox epsText = (TextBox)this.FormView1.FindControl("txtEPS1");
        TextBox epsText2 = (TextBox)this.FormView1.FindControl("txtEPS2");

        TextBox ATL_Name = (TextBox)this.FormView1.FindControl("ATL_Name");
        TextBox ATL_Name2 = (TextBox)this.FormView1.FindControl("ATL_Name2");
        TextBox ATL_Text1 = (TextBox)this.FormView1.FindControl("ATL_Text1");
        TextBox ATL_Text2 = (TextBox)this.FormView1.FindControl("ATL_Text2");

        HiddenField hdfGroupID1 = (HiddenField)this.FormView1.FindControl("GroupID");
        HiddenField hdfGroupID2 = (HiddenField)this.FormView1.FindControl("GroupID2");
        int groupID1 = Convert.ToInt32(hdfGroupID1.Value);
        int groupID2 = Convert.ToInt32(hdfGroupID2.Value);
       
        string aName = ATL_Name.Text.ToString();
        string aName2 = ATL_Name2.Text.ToString();
        string aText1 = ATL_Text1.Text.ToString();
        string aText2 = ATL_Text2.Text.ToString();

        
        if (file1.PostedFile.FileName != "")
        {
            filename = file1.PostedFile.FileName;
            if (file1.PostedFile.ContentLength > 10524288)
            {
                //Keine Dateien zulassen, die grösser wie 500KB sind
                filename = "";
                string scriptString1 = "<script language=JavaScript> " + "alert('Die Datei ist zu gross!');</script>";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString1))
                     Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"script", scriptString1);
            }
            else
            {
                string[] xName = file1.PostedFile.FileName.ToString().Split('.');
                string extension = xName[1].ToString();
                //filename = ATT_ID + "_Image1." + extension.ToString();//ART_ID + "_" + THM_ID + "_Image1." + extension.ToString();

                filename = ATT_ID + "_1_" +Path.GetFileName(file1.PostedFile.FileName.Trim());
                string tempFile = "temp_" + Path.GetFileName(file1.PostedFile.FileName);
                file1.PostedFile.SaveAs(FilePath + tempFile);
                ResizeImage.ImageSave(tempFile, filename, 365);
                File.Delete(FilePath + tempFile);
                
                //file1.PostedFile.SaveAs(FilePath + filename);
                //ResizeImage.ImageSave(file1.PostedFile.FileName, filename, 365);


            }
        }else{
            if (epsText.Text.Trim() != "" && ATT_ID != 7)
            {
                filename = epsText.Text.ToString().Trim() + ".jpg";       
                string filenameCopy = epsText.Text.ToString().Trim();
                ResizeImage.ImageCopy(filenameCopy, groupID1, 365);
            }

           
        }



        if (file2.PostedFile.FileName != "")
        {
            filename2 = file2.PostedFile.FileName;
            if (file2.PostedFile.ContentLength > 10524288)
            {
                //Keine Dateien zulassen, die grösser wie 500KB sind
                filename2 = "";
                string scriptString2 = "<script language=JavaScript> " + "alert('Die Datei ist zu gross!');</script>";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString2))
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", scriptString2);
            }
            else
            {
                string[] xName2 = file2.PostedFile.FileName.ToString().Split('.');
                string extension2 = xName2[1].ToString();
                //filename2 = ATT_ID + "_Image2." + extension2.ToString();
                filename2 = ATT_ID + "_2_" + Path.GetFileName(file2.PostedFile.FileName);
                string tempFile2 = "temp_" + Path.GetFileName(file2.PostedFile.FileName);
                file2.PostedFile.SaveAs(FilePath + tempFile2);
                ResizeImage.ImageSave(tempFile2, filename2, 365);
                File.Delete(FilePath + tempFile2);


                //file2.PostedFile.SaveAs(FilePath + filename2);
                //ResizeImage.ImageSave(file2.PostedFile.FileName, filename2, 365);
            }
        }
        else
        {
            if (epsText2.Text.Trim() != "" && ATT_ID != 7)
            {
                filename2 = epsText2.Text.ToString().Trim() + ".jpg";
                string filenameCopy2 = epsText2.Text.ToString().Trim();
                ResizeImage.ImageCopy(filenameCopy2, groupID2, 365);
            }
         
        }


        ATT_ID = DataAccess.insertAttribute(ART_ID,THM_ID, aName, aName2,filename, filename2, aText1, aText2, AGR_ID,groupID1,groupID2, RBT_ID);
        
     
        string scriptString = "<script language=JavaScript> " +
       "window.opener.document.forms[0].submit();window.opener.opener.document.forms[0].submit();window.close();</script>";


        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
             Page.ClientScript.RegisterClientScriptBlock(this.GetType(),"script", scriptString);
        
        //FormView1.ChangeMode(FormViewMode.Edit);
        dispImage1 = (Image)FormView1.FindControl("DispImage1");
        dispImage2 = (Image)FormView1.FindControl("DispImage2");
        dispImage1.ImageUrl = "CreateThumbNail.aspx?Image=" + FilePath + filename;
        dispImage2.ImageUrl = "CreateThumbNail.aspx?Image=" + FilePath + filename2;
       
    }
    protected void UpdateCancelButton_Click(object sender, EventArgs e)
    {
        string scriptString = "<script language=JavaScript> " +
       "window.close();</script>";


        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                               "script", scriptString);
        }
    }
    protected void InsertCancelButton_Click(object sender, EventArgs e)
    {
        string scriptString = "<script language=JavaScript> " +
              "window.close();</script>";


        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                               "script", scriptString);
        }
    }
        
    protected void FormView1_DataBound(object sender, EventArgs e)
    {

        Button btnKat1 = (Button)FormView1.FindControl("btnKat1");
        btnKat1.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=37&field=1','showImage', 500,600);return false");

        Button btnKat2 = (Button)FormView1.FindControl("btnKat2");
        btnKat2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=37&field=2','showImage', 500,600);return false");

        Button btnIso1 = (Button)FormView1.FindControl("btnIso1");
        btnIso1.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=43&field=1','showImage', 500,600);return false");

        Button btnIso2 = (Button)FormView1.FindControl("btnIso2");
        btnIso2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=43&field=2','showImage', 500,600);return false");

        Button btnPic1 = (Button)FormView1.FindControl("btnPic1");
        btnPic1.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=29&field=1','showImage', 500,600);return false");

        Button btnPic1_2 = (Button)FormView1.FindControl("btnPic1_2");
        btnPic1_2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=30&field=1','showImage', 500,600);return false");

        Button btnPic1_3 = (Button)FormView1.FindControl("btnPic1_3");
        btnPic1_3.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=31&field=1','showImage', 500,600);return false");
   
        Button btnPic2 = (Button)FormView1.FindControl("btnPic2");
        btnPic2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=29&field=2','showImage', 500,600);return false");

        Button btnPic2_2 = (Button)FormView1.FindControl("btnPic2_2");
        btnPic2_2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=30&field=2','showImage', 500,600);return false");

        Button btnPic2_3 = (Button)FormView1.FindControl("btnPic2_3");
        btnPic2_3.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=31&field=2','showImage', 500,600);return false");

        Button btnRef1 = (Button)FormView1.FindControl("btn_Ref");
        btnRef1.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=99&field=1','showImage', 500,600);return false");

        Button btnRef2 = (Button)FormView1.FindControl("btn_Ref2");
        btnRef2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=99&field=2','showImage', 500,600);return false");

              
        Button btnProdFotos = (Button)FormView1.FindControl("btn_ProductFotos");
        btnProdFotos.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=48&field=1','showImage', 500,600);return false");

        Button btnProdFotos2 = (Button)FormView1.FindControl("btn_ProductFotos2");
        btnProdFotos2.Attributes.Add("onclick", "openWindow('SelectDialogCatalog.aspx?artID=" + ART_ID + "&groupID=48&field=2','showImage', 500,600);return false");

      
        if (ATT_ID > -1)
        {
            dispImage1 = (Image)FormView1.FindControl("DispImage1");
            dispImage2 = (Image)FormView1.FindControl("DispImage2");

            int groupID1 = Convert.ToInt32(DataBinder.Eval(this.FormView1.DataItem, "ATT_ImgGroup1").ToString());
            int groupID2 = Convert.ToInt32(DataBinder.Eval(this.FormView1.DataItem, "ATT_ImgGroup2").ToString());
            string img1 = DataBinder.Eval(this.FormView1.DataItem, "ATT_Image1").ToString();
            string img2 = DataBinder.Eval(this.FormView1.DataItem, "ATT_Image2").ToString();

            

            if (img1 != "")
            {

                if (THM_ID == 7)
                    FilePath = FilePath2 + "references/";
                if (groupID1 == 48)
                    FilePath = FilePath2 + "Schulungsbilder/";

            

                dispImage1.ImageUrl = "CreateThumbNail.aspx?Image=" + FilePath + img1;
                string sImage1 = "DispImage.aspx?sImage=" + img1;
                dispImage1.Attributes.Add("onclick", "openWindow('" + sImage1 + "','showImage', 500,600);return false");
            }
            else
            {
                dispImage1.ImageUrl = "APP_Images/ts.gif";
            }
            if (img2 != "")
            {
                if (THM_ID == 7)
                    FilePath = FilePath2 + "references/";
                if (groupID2 == 48)
                    FilePath = FilePath2 + "Schulungsbilder/";


                dispImage2.ImageUrl = "CreateThumbNail.aspx?Image=" + FilePath + img2;
                string sImage2 = "DispImage.aspx?sImage=" + img2;
                dispImage2.Attributes.Add("onclick", "openWindow('" + sImage2 + "','showImage', 500,600);return false");
            }
            else{
                dispImage2.ImageUrl = "APP_Images/ts.gif";
            }
        
           
            
        }
    }
    protected void FormView1_ItemCreated(object sender, EventArgs e)
    {
        Label attLabel1 = (Label)this.FormView1.FindControl("lblAttribute");
        Label attLabel2 = (Label)this.FormView1.FindControl("lblAttribute2");
        TextBox attText2 = (TextBox)this.FormView1.FindControl("ATL_Name2");
        HtmlTableRow rowName2 = (HtmlTableRow)this.FormView1.FindControl("trName2");
        if(rowName2 != null)
            rowName2.Visible = false;

        if (THM_ID == 6)
        {
            rowName2.Visible = true;
            attLabel1.Text = "Problem:";

        }
        
      
    }
    public bool ThumbnailCallback()
    {
        return false;
    }
    protected void btn_DeleteImage1_Click(object sender, ImageClickEventArgs e)
    {
        HiddenField hdATT_ID = (HiddenField)FormView1.FindControl("ATT_ID");
        dispImage1 = (Image)FormView1.FindControl("DispImage1");
        int attID = Convert.ToInt32(hdATT_ID.Value);
        DataAccess.deleteImage(attID, 1);
        dispImage1.ImageUrl = "App_Images/ts.gif";
    }
    protected void btn_DeleteImage2_Click(object sender, ImageClickEventArgs e)
    {
        HiddenField hdATT_ID = (HiddenField)FormView1.FindControl("ATT_ID");
        dispImage2 = (Image)FormView1.FindControl("DispImage2");
        int attID = Convert.ToInt32(hdATT_ID.Value);
        DataAccess.deleteImage(attID, 2);
        dispImage2.ImageUrl = "App_Images/ts.gif";
    }
}
