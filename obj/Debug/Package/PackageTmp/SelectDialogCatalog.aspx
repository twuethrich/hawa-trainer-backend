﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SelectDialogCatalog" Codebehind="SelectDialogCatalog.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
     <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        select{font-size:9pt;background-color:yellow;}
        a {color:red;}
    </style>
     <script type="text/javascript" language="javascript">
        	function selectPicture(picID,field,groupID){
        	   var fld = field;
        	
			   	if(fld == 1){
				    opener.document.forms[0].FormView1_txtEPS1.value = picID;
				    opener.document.forms[0].FormView1_GroupID.value = groupID;
				   // opener.document.forms[0].FormView1_FileUpload1.disabled = true;
				}else{
				    opener.document.forms[0].FormView1_txtEPS2.value = picID;
				    opener.document.forms[0].FormView1_GroupID2.value = groupID;
				    //opener.document.forms[0].FormView1_FileUpload2.disabled = true;
				}
		       
		   		window.close();
				 //document.forms[0].elements["hf_picID"].value = picID;
				 //document.forms[0].submit();
				 //opener.__doPostBack();
				 
			}
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:HiddenField ID="hf_picID" runat="server" />
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="HWA_getArticleAndGroup" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="DE" Name="LanguageID" Type="String" />
                <asp:QueryStringParameter DefaultValue="1" Name="ArticleID" QueryStringField="artID"
                    Type="Decimal" />
                <asp:QueryStringParameter DefaultValue="37" Name="GroupID" QueryStringField="groupID"
                    Type="Decimal" />
                <asp:Parameter DefaultValue="-1" Name="Accessoire" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="HWA_getSubArticleObjects" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="DE" Name="LanguageID" Type="String" />
                <asp:QueryStringParameter DefaultValue="1" Name="ArticleID" QueryStringField="artID"
                    Type="Decimal" />
                <asp:QueryStringParameter DefaultValue="43" Name="GroupID" QueryStringField="groupID"
                    Type="Decimal" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="SELECT AR.REF_ID as OBJ_ID, rl.REL_Object + '/' + RL.REL_Place AS OBJ_Name, substring(R.REF_FileGUID,0,len(R.REF_FileGUID)-3)  as RefImage FROM  CLAPP_Article_Reference AR LEFT JOIN&#13;&#10;CLAPP_Reference R ON AR.REF_ID = R.REF_ID LEFT JOIN&#13;&#10;CLAPP_ReferenceLang RL ON ar.REF_ID = RL.REF_ID AND RL.LNG_ID ='DE'&#13;&#10;WHERE AR.ART_ID =  @ArtID AND R.REF_FileGUID IS NOT NULL">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="0" Name="ArtID" QueryStringField="artID" />
            </SelectParameters>
        </asp:SqlDataSource><asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="ESH_getSubArticleObjects" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="DE" Name="LanguageID" Type="String" />
                <asp:QueryStringParameter DefaultValue="1" Name="ArticleID" QueryStringField="artID"
                    Type="Decimal" />
            </SelectParameters>
        </asp:SqlDataSource>
    
    </div>
       <div style="overflow:scroll;height:580px;">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="OBJ_ID"
            DataSourceID="SqlDataSource1" OnRowDataBound="GridView1_RowDataBound" ForeColor="#333333" GridLines="None" Width="90%" >
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image ID="ImgPreview" runat="server" />
                    </ItemTemplate>
                    <ItemStyle BackColor="White" Width="40%" />
                </asp:TemplateField>

                <asp:BoundField DataField="OBJ_ID" HeaderText="OBJ_ID" InsertVisible="False" ReadOnly="True" Visible="False"
                    SortExpression="OBJ_ID" />
                
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkSelect" runat="server" NavigateUrl="#" Text='<%# Eval("OBJ_Name") %>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="60%" />
                </asp:TemplateField>
            </Columns>
               <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#DEDEDE" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        </div>
    </form>
</body>
</html>
