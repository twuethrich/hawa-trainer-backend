﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AccessoireList" Codebehind="AccessoireList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
     <script src="JScript.js" type="text/Jscript"></script>
     <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        select{font-size:9pt;background-color:yellow;}
    </style>
</head>
<body onload="window.focus();">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td style="font-weight: bold; font-size: 12pt; width: 300px">
                    Attribute</td>
                <td align="right" rowspan="2" style="width: 180px">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Images/logo.jpg" /></td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <span style="font-family: Webdings">8</span>
                    <asp:Label ID="Label1" runat="server" Font-Size="10pt" Text="Label"></asp:Label>&nbsp;<br />
                    <span style="font-family: Webdings">8</span>
                    <asp:Label ID="Label2" runat="server" Font-Size="10pt" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;<div style="overflow: auto; width: 490px; height: 490px">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                DataKeyNames="AGR_ID" DataSourceID="SqlDataSource1" EnableViewState="False" ForeColor="#333333"
                GridLines="None" OnRowDataBound="GridView1_RowDataBound" Width="95%">
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="AGR_ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                        SortExpression="AGR_ID" />
                    <asp:BoundField DataField="AGL_Name" HeaderText="Accessoire" SortExpression="AGL_Name" />
                    <asp:BoundField DataField="Holz" HeaderText="Holz" SortExpression="Holz" />
                    <asp:BoundField DataField="Glas" HeaderText="Glas" SortExpression="Glas" />
                    <asp:BoundField DataField="Metall" HeaderText="Metall" SortExpression="Metall" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/App_Images/edit.gif" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle BackColor="#DEDEDE" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="EDU_getAccessoireList" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="ART_ID" QueryStringField="artID"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        &nbsp;&nbsp;
    </form>
</body>
</html>
