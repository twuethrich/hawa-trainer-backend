﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SelectPhotoDialog" Codebehind="SelectPhotoDialog.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
            ForeColor="#333333" GridLines="None" OnRowDataBound="GridView1_RowDataBound"
            Width="90%">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#DEDEDE" />
            <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField></asp:TemplateField>
                <asp:BoundField DataField="OBJ_Name" HeaderText="OBJ_Name" SortExpression="OBJ_Name" />
            </Columns>
        </asp:GridView>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="HWA_getArticleAndGroup" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="DE" Name="LanguageID" Type="String" />
                <asp:QueryStringParameter DefaultValue="1" Name="ArticleID" QueryStringField="artID"
                    Type="Decimal" />
                <asp:QueryStringParameter DefaultValue="29" Name="GroupID" QueryStringField="groupID"
                    Type="Decimal" />
                <asp:Parameter DefaultValue="-1" Name="Accessoire" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
