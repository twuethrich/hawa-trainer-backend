﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AttributeDetail" Codebehind="AttributeDetail.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
     <script src="JScript.js" type="text/Jscript"></script>
    <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        button{background-color:yellow;
        
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
            <table style="width: 500px;">
                <tr>
                    <td style="width: 256px;font-size: 12pt;font-weight:bold;">
                        Attributedetail</td>
                    <td align="right" rowspan="2" style="width: 111px;">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Images/logo.jpg" /></td>
                </tr>
                <tr>
                    <td style="width: 256px; height: 53px;">
                        <span style="font-family: Webdings">8</span>
                        <asp:Label ID="Label1" Font-Names="Arial" Font-Size="10pt" runat="server" Text="Label"></asp:Label>&nbsp;<br />
                        <span style="font-family: Webdings">8</span>
                        <asp:Label ID="Label2" Font-Names="Arial" Font-Size="10pt" runat="server" Text="Label"></asp:Label>&nbsp;<br />
                        <span style="font-family: Webdings">8</span>
                        <asp:Label ID="Label3" Font-Names="Arial" Font-Size="10pt" runat="server" Text="Label" ></asp:Label>
                        </td>
                </tr>
            </table>
       
       
        <hr style="width:100%; height: 6px; background-color: #ee3e41;" />
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="ATT_ID" DataSourceID="SqlDataSource1"
            DefaultMode="Edit" Width="480px" OnDataBound="FormView1_DataBound" OnItemCreated="FormView1_ItemCreated">
            <EditItemTemplate>
                <table style="width: 100%;" >
                    <tr>
                        <td style="width: 100px; height:24px; ">
                            <asp:Label ID="lblAttribute" runat="server" Text="Attribut:"></asp:Label></td>
                        <td style="height: 24px; width: 22px;"></td>
                        <td style="width: 295px; height: 24px;">
                        <asp:TextBox ID="ATL_NAME" runat="server" Text='<%# Bind("ATL_NAME") %>' Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                     <tr runat="server" id="trName2" visible="false">
                        <td style="width: 100px; height:24px; ">
                            <asp:Label ID="lblAttribute2" runat="server" Text="Lösung:"></asp:Label></td>
                        <td style="height: 24px; width: 22px;"></td>
                        <td style="width: 295px; height: 24px;">
                        <asp:TextBox ID="ATL_NAME2" runat="server" Text='<%# Bind("ATL_Name2") %>' Columns="50" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Bild1:</td>
                        <td style="width: 22px"></td>
                        <td style="width: 295px">
                            <asp:TextBox ID="txtEPS1" runat="server"></asp:TextBox>
                            <asp:Button ID="btnKat1" runat="server" Text="Kat" />
                            <asp:Button ID="btnIso1" runat="server" Text="Iso" />
                            <asp:Button ID="btnPic1" runat="server" Text="F1" />
                            <asp:Button ID="btnPic1_2" runat="server" Text="F2" />
                            <asp:Button ID="btnPic1_3" runat="server" Text="F3" />
                            <asp:Button ID="btn_Ref" runat="server" Text="Ref" />
                            <asp:Button ID="btn_ProductFotos" runat="server" Text="F4" /><br />
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <asp:ImageButton ID="btn_DeleteImage1" runat="server" ImageUrl="~/App_Images/trash.gif"
                                OnClick="btn_DeleteImage1_Click" />&nbsp;<br />
                            &nbsp;<asp:ImageButton ID="DispImage1" runat="server" Height="80px" ImageUrl="~/App_Images/ts.gif" Width="80px" ToolTip="Wenn Sie auf das Vorschaubild klicken, können Sie das Bild in der Originalgrösse betrachten" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">Bild2:</td>
                        <td style="width: 22px"></td>
                        <td style="width: 295px">
                            <asp:TextBox ID="txtEPS2" runat="server"></asp:TextBox>
                            <asp:Button ID="btnKat2" runat="server" Text="Kat" />
                            <asp:Button ID="btnIso2" runat="server" Text="Iso" />
                            <asp:Button ID="btnPic2" runat="server" Text="F1" />
                             <asp:Button ID="btnPic2_2" runat="server" Text="F2" />
                            <asp:Button ID="btnPic2_3" runat="server" Text="F3" /><br /><asp:Button ID="btn_Ref2" runat="server" Text="Ref" />
                            <asp:Button ID="btn_ProductFotos2" runat="server" Text="F4" />
                            <br />
                            <asp:FileUpload ID="FileUpload2" runat="server" />
                            <asp:ImageButton ID="btn_DeleteImage2" runat="server" ImageUrl="~/App_Images/trash.gif"
                                OnClick="btn_DeleteImage2_Click" /><br />
                            &nbsp;<asp:ImageButton ID="DispImage2" runat="server" Height="80px" ImageUrl="~/App_Images/ts.gif" Width="80px" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" >
                            Text1:
                        </td>
                        <td style="width: 22px" >
                        </td>
                        <td style="width: 295px">
                <asp:TextBox ID="ATL_Text1" runat="server" Text='<%# Bind("ATL_Text1") %>' Height="50px" Rows="20" TextMode="MultiLine" Width="270px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Text2:
                        </td>
                        <td style="width: 22px" >
                        </td>
                        <td style="width: 295px" >
                <asp:TextBox ID="ATL_Text2" runat="server" Text='<%# Bind("ATL_Text2") %>' Height="50px" Rows="20" TextMode="MultiLine" Width="270px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 22px">
                        </td>
                        <td style="width: 295px" align="right">
                            <br />
                            <br />
                            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" OnClick="UpdateCancelButton_Click"
                                Text="Abbrechen" />
                            <asp:Button ID="btnUpdate" runat="server" OnClick="UpdateButton_Click"
                                Text="Aktualisieren" /></td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td style="height: 26px" align="right" colspan="3">
                <asp:HiddenField ID="ATT_ID" runat="server" Value='<%# Eval("ATT_ID") %>' />
                            <asp:HiddenField ID="GroupID" runat="server" Value="0" /><asp:HiddenField ID="GroupID2" runat="server" Value="0" />
                            &nbsp; &nbsp;&nbsp;</td>
                    </tr>
                </table>
                <br />
               
            </EditItemTemplate>
            <InsertItemTemplate>
                <table style="width: 100%">
                     <tr>
                        <td style="width: 100px; height:24px; ">
                            <asp:Label ID="lblAttribute" runat="server" Text="Attribut:"></asp:Label></td>
                        <td style="height: 24px; width: 137px;"></td>
                        <td style="width: 285px; height: 24px;">
                        <asp:TextBox ID="ATL_NAME" runat="server" Text='<%# Bind("ATL_NAME") %>' Columns="50" Rows="2" TextMode="MultiLine" Width="273px" Height="30px"></asp:TextBox></td>
                    </tr>
                     <tr runat="server" id="trName2" visible="false">
                        <td style="width: 100px; height:24px; ">
                            <asp:Label ID="lblAttribute2" runat="server" Text="Lösung:"></asp:Label></td>
                        <td style="height: 24px; width: 137px;"></td>
                        <td style="width: 285px; height: 24px;">
                        <asp:TextBox ID="ATL_NAME2" runat="server" Text='<%# Bind("ATL_Name2") %>' Columns="50" Rows="2" TextMode="MultiLine" Width="273px" Height="30px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 28px;">
                            Bild1:</td>
                        <td style="width: 137px; height: 28px">
                        </td>
                        <td style="height: 28px; width: 285px;">
                            <asp:TextBox ID="txtEPS1" runat="server"></asp:TextBox>
                            <asp:Button ID="btnKat1" runat="server" Text="Kat" />
                            <asp:Button ID="btnIso1" runat="server" Text="Iso" />
                            <asp:Button ID="btnPic1" runat="server" Text="F1" />
                            <asp:Button ID="btnPic1_2" runat="server" Text="F2" />
                            <asp:Button ID="btnPic1_3" runat="server" Text="F3" />
                            <asp:Button ID="btn_Ref" runat="server" Text="Ref" />
                            <asp:Button ID="btn_ProductFotos" runat="server" Text="F4" /><br />
                            &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" /><br />
                            &nbsp;<asp:ImageButton ID="DispImage1" runat="server" Height="80px" ImageUrl="~/App_Images/ts.gif"
                                Width="80px" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 28px;">
                            Bild2:</td>
                        <td style="width: 137px; height: 28px">
                        </td>
                        <td style="height: 28px; width: 285px;">
                              <asp:TextBox ID="txtEPS2" runat="server"></asp:TextBox>
                            <asp:Button ID="btnKat2" runat="server" Text="Kat" />
                            <asp:Button ID="btnIso2" runat="server" Text="Iso" />
                            <asp:Button ID="btnPic2" runat="server" Text="F1" />
                            <asp:Button ID="btnPic2_2" runat="server" Text="F2" />
                            <asp:Button ID="btnPic2_3" runat="server" Text="F3" />
                            <asp:Button ID="btn_Ref2" runat="server" Text="Ref" />
                            <asp:Button ID="btn_ProductFotos2" runat="server" Text="F4" /><br />
                              &nbsp;<asp:FileUpload ID="FileUpload2" runat="server"  /><br />
                              &nbsp;<asp:ImageButton ID="DispImage2" runat="server" Height="80px" ImageUrl="~/App_Images/ts.gif"
                                Width="80px" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 28px;">
                            Text1:</td>
                        <td style="width: 137px; height: 28px">
                        </td>
                        <td style="height: 28px; width: 285px;">
                <asp:TextBox ID="ATL_Text1" runat="server" Text='<%# Bind("ATL_Text1") %>' Columns="50" Height="50px" Rows="20" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 29px;">
                            Text2:</td>
                        <td style="width: 137px; height: 29px">
                        </td>
                        <td style="height: 29px; width: 285px;">
                <asp:TextBox ID="ATL_Text2" runat="server" Text='<%# Bind("ATL_Text2") %>' Columns="50" Height="50px" Rows="20" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="height: 7px;" colspan="3">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ATL_NAME"
                                ErrorMessage="Der Attributname ist erforderlich" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Button ID="Button1" runat="server" CommandName="Cancel" OnClick="InsertCancelButton_Click"
                                Text="Abbrechen" OnClientClick="window.close();" />
                            <asp:Button ID="Button2" runat="server" OnClick="InsertButton_Click"
                                Text="Speichern" /></td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td style="height: 26px" align="right" colspan="3">
                <asp:HiddenField ID="ATT_ID" runat="server" Value='<%# Eval("ATT_ID") %>' />
                 <asp:HiddenField ID="ART_ID" runat="server" Value='<%# Eval("ART_ID") %>' />
                  <asp:HiddenField ID="THM_ID" runat="server" Value='<%# Eval("THM_ID") %>' />    
                  <asp:HiddenField ID="GroupID" runat="server" Value="0" />
                  <asp:HiddenField ID="GroupID2" runat="server" Value="0" />
                            &nbsp; &nbsp;&nbsp;</td>
                    </tr>
                </table>
            </InsertItemTemplate>
           
        </asp:FormView>
        <span style="font-size: 11pt; font-family: Wingdings"></span><span style="font-family: Webdings">
        </span>
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="EDU_getAttributeDetail" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="ATT_ID" QueryStringField="attID"
                    Type="Int32" />
                <asp:QueryStringParameter DefaultValue="1" Name="ART_ID" QueryStringField="artID"
                    Type="Int32" />
                <asp:QueryStringParameter DefaultValue="1" Name="THM_ID" QueryStringField="thmID"
                    Type="Int32" />
                <asp:QueryStringParameter DefaultValue="de" Name="LanguageID" 
                    QueryStringField="langID" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        
    </form>
</body>
</html>
