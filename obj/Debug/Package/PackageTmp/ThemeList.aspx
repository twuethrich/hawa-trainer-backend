﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ThemeList" Codebehind="ThemeList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
    <script src="JScript.js" type="text/Jscript"></script>
    <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        select{font-size:9pt;background-color:yellow;}
    </style>
      <script type="text/javascript" language="javascript">
         function setProductTheme(artID,thmID){
            del = 0;
             if(!document.getElementById("chk" + thmID).checked){
			     del = 1;
			 }	
			
			document.location.href ="ThemeList.aspx?artID="+artID+"&thmID="+thmID+"&del="+del;
			//opener.__doPostBack();
		
				 
		}
    </script>
</head>
<body style="color: #000000" >
    <form id="form1" runat="server">
      
        <table border="0">
        <tr>
            <td style="width: 500px">
                <span style="font-size: 12pt;font-weight:bold;">
                <strong>Themen&nbsp;  <br />
                </span>
            </td>
            <td align="right" rowspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Images/logo.jpg" /></td>
         </tr>
        <tr><td style="width: 500px">
            <span style="font-family: Webdings">8</span>
            <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="10pt"
            Text="Label"></asp:Label>
         </td></tr>
        </table>
        &nbsp;
      
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="THM_ID" Width="100%" EnableViewState="False">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="THM_ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                    SortExpression="THM_ID" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="THL_NAME" HeaderText="Thema" SortExpression="THL_NAME" >
                    <ItemStyle Width="60%" />
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="isConfig" HeaderText="isConfig" SortExpression="isConfig" Visible="False"/>
                <asp:BoundField DataField="hasConfig" HeaderText="zugeordnete Attribute" SortExpression="hasConfig">
                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                    <HeaderStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="hasAccessoire" HeaderText="hasAccessoire" SortExpression="hasAccessoire"
                    Visible="False" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/App_Images/edit.gif"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#DEDEDE" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="EDU_getThemeList" SelectCommandType="StoredProcedure" UpdateCommand="EDU_UpdateProductTheme" UpdateCommandType="StoredProcedure">
            <UpdateParameters>
                <asp:Parameter Name="ART_ID" Type="Int32" />
                <asp:Parameter Name="THM_ID" Type="Int32" />
                <asp:Parameter Name="del" Type="Int32" />
            </UpdateParameters>
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="ART_ID" QueryStringField="artID"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        &nbsp;
    </form>
</body>
</html>
