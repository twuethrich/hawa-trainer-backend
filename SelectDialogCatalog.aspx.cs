﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SelectDialogCatalog : System.Web.UI.Page
{
    public int fieldNr = 1;
    public int groupID = 37;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        
        if (Request["field"] != null)
            fieldNr = Convert.ToInt16(Request.QueryString.Get("field"));
        if (Request["groupID"] != null)
            groupID = Convert.ToInt16(Request.QueryString.Get("groupID"));

        if (groupID == 43)
        {
            GridView1.DataSourceID.Remove(0);
            GridView1.DataSourceID = SqlDataSource2.ID;
            GridView1.DataBind();
        }
        if (groupID == 48)
        {
            GridView1.DataSourceID.Remove(0);
            GridView1.DataSourceID = SqlDataSource4.ID;
            GridView1.DataBind();
        }
        if (groupID == 99)
        {
            GridView1.DataSourceID.Remove(0);
            GridView1.DataSourceID = SqlDataSource3.ID;
            GridView1.DataBind();
        }

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string FilePath = ConfigurationManager.AppSettings["ObjectPath2"];

        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            string picID = DataBinder.Eval(e.Row.DataItem, "OBJ_Name").ToString();
            if(groupID == 99)
                picID = DataBinder.Eval(e.Row.DataItem, "RefImage").ToString();
            
            HyperLink lnkSelect = (HyperLink)e.Row.FindControl("lnkSelect");
            lnkSelect.Attributes.Add("onclick", "selectPicture('" + picID + "'," + fieldNr + "," + groupID + ")");
                                    
            Image imgPreview = (Image)e.Row.FindControl("ImgPreview");

            switch (groupID)
            {
                case 37:
                    imgPreview.ImageUrl = "getImage.aspx?typ=1&id=" + picID; break;
                case 43:
                    imgPreview.ImageUrl = "getImage.aspx?typ=2&id=" + picID; break;
                case 29:
                    imgPreview.ImageUrl = "getImage.aspx?typ=3&id=" + picID; break;
                case 30:
                    imgPreview.ImageUrl = "getImage.aspx?typ=4&id=" + picID; break;
                case 31:
                    imgPreview.ImageUrl = "getImage.aspx?typ=5&id=" + picID; break;
                case 48:
                    imgPreview.ImageUrl = "getImage.aspx?typ=7&id=" + picID; break;
                //Referenzen
                case 99:
                    imgPreview.ImageUrl = "getImage.aspx?typ=6&id=" + picID; break;

            }

        }



    }
}
