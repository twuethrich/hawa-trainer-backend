﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ThemeList : System.Web.UI.Page
{
    public int artID;
    int thmID = -1;
    public int isConfig = 1;
    int del = -1;
    
    string aName;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params.Get("aName") != null)
        {
            aName = Request.QueryString.Get("aName").ToString();
            Session.Add("Product", aName);
           
        }
        Label1.Text = Session["Product"].ToString();

        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Get("artID") != null)
            {
                artID = Convert.ToInt32(Request.QueryString.Get("artID"));
                Session.Add("artID", artID);
            }
        }
        

        if (Convert.ToInt32(Request.QueryString.Get("thmID")) > 0)
        {
            thmID = Convert.ToInt32(Request.QueryString.Get("thmID"));
            del = Convert.ToInt32(Request.Params.Get("del"));
            DataAccess.updateProductTheme(artID, thmID, del);
        }
        

       
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            int thmID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "THM_ID"));
            string thema = DataBinder.Eval(e.Row.DataItem, "THL_Name").ToString();
            int isConfig = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "isConfig"));
            int hasConfig = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "hasConfig"));
            int hasAccessoire = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "hasAccessoire"));
            
            artID = Convert.ToInt32(Session["artID"]);

            string xLink = "AttributeList.aspx?thmID=" + thmID + "&artID=" + artID + "&thema=" + thema;
            
            if (thmID == 3)
                xLink = "AccessoireList.aspx?&artID=" + artID + "&thema=" + thema +"&thmID="+thmID;

            if (isConfig == 1 || (thmID == 3 &&  hasAccessoire > 0))
            {
                e.Row.Cells[0].Text = "<input type=checkbox id='chk" + thmID + "' checked onclick='setProductTheme(" + artID + "," + thmID + ")'>";
                btnEdit.Attributes.Add("onclick", "openWindow('" + xLink + "','attribute', 500,600);return false");
            }
            else
            {
                e.Row.Cells[0].Text = "<input type=checkbox id='chk" + thmID + "' onclick='setProductTheme(" + artID + "," + thmID + ")'>";
                btnEdit.Attributes.Add("onclick", "alert('Dieses Thema muss zuerst zugeordnet werden!')");
            }
            e.Row.Cells[4].Text = "<img src=APP_Images/bullet_round_red.gif Border=0>";
            if (hasConfig > 0)
                e.Row.Cells[4].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";

            if (thmID == 3 &&  hasAccessoire > 0)
                e.Row.Cells[4].Text = "<img src=APP_Images/bullet_round_green.gif Border=0>";
            

        }
    }
    
    
  
}
