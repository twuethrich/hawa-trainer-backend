﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AccessoireList : System.Web.UI.Page
{
    public int artID;
    public int thmID;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Get("thmID") != null)
            {
                Session.Add("thmID", Convert.ToInt32(Request.QueryString.Get("thmID")));
                thmID = Convert.ToInt32(Request.QueryString.Get("thmID"));
            }
            if (Request.QueryString.Get("artID") != null)
            {
                Session.Add("artID", Convert.ToInt32(Request.QueryString.Get("artID")));
                artID = Convert.ToInt32(Request.QueryString.Get("artID"));
            }
            if (Request.QueryString.Get("thema") != null)
                Session.Add("Theme", Request.QueryString.Get("thema"));
        }
        
        Label2.Text = Session["Theme"].ToString();
        Label1.Text = Session["Product"].ToString();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {

            int holz = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Holz"));
            int metall = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Metall"));
            int glas = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Glas"));
            thmID = Convert.ToInt32(Session["thmID"]);
            artID = Convert.ToInt32(Session["artID"]);


            e.Row.Cells[2].Text = "";
            e.Row.Cells[3].Text = "";
            e.Row.Cells[4].Text = "";
            if (holz > 0)
                e.Row.Cells[2].Text = "x";
            if (glas > 0)
                e.Row.Cells[3].Text = "x";
            if (metall > 0)
                e.Row.Cells[4].Text = "x";



            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");

            int attID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ATT_ID"));
            int agrID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AGR_ID"));

            string groupName = DataBinder.Eval(e.Row.DataItem, "AGL_Name").ToString();
            string xLink = "AttributeDetail.aspx?attID=" + attID + "&attribute=Accessoire&agrID=" + agrID + "&artID=" + artID + "&thmID=" + thmID;
            btnEdit.Attributes.Add("onclick", "openWindow('" + xLink + "','newAttribute', 500,650);return false");

        }
    }
}
