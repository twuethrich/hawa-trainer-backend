﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;



public partial class getImage : System.Web.UI.Page
{
    public static string strConnString = ConfigurationManager.ConnectionStrings["CLB_HawaConnectionString"].ConnectionString;
    public string FilePath = ConfigurationManager.AppSettings["ObjectPath"];
    public string FilePath2 = ConfigurationManager.AppSettings["ObjectPath2"];
    protected void Page_Load(object sender, EventArgs e)
    {

        string name = "";
        string path = "";
        int typ = 0;

        try
        {
            string id = Request["id"];
            if(Request.QueryString.Get("typ") != null)
                typ = Convert.ToInt32(Request["typ"]);
            
            int group = Convert.ToInt32(Request["group"]);

            name = id;

            switch (typ){
                case 0:
                    path = FilePath + name; break;
                case 1:
                    path = FilePath2 + @"Katalogzeichnungen\thumbnails\" + name + ".jpg"; break;
                case 2:
                    path = FilePath2 + @"Isometrische_Darstellung_Artikel\thumbnails\" + name + ".jpg"; break;
                case 3:
                    path = FilePath2 + @"Systembild 1\thumbnails\" + name + ".jpg"; break;
                case 4:
                    path = FilePath2 + @"Systembild 2\thumbnails\" + name + ".jpg"; break;
                case 5:
                    path = FilePath2 + @"Systembild 3\thumbnails\" + name + ".jpg"; break;
                case 6:
                    path = FilePath2 + @"references\" + name + ".jpg"; break;
                case 7:
                    path = FilePath2 + @"Schulungsbilder\thumbnails\" + name + ".jpg"; break;
                default:
                        path = FilePath + name; break;

            }
            
           
            Response.AddHeader("content-disposition", "attachment; filename=" + name);
            Response.WriteFile(path);
            //Response.End();
        }
        catch (Exception)
        {
            
                        
        }




 

   
}

}
