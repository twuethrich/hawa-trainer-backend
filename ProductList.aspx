﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ProductList" Codebehind="ProductList.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
    <script src="JScript.js" type="text/Jscript"></script>
    <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        select{font-size:9pt;background-color:yellow;}
    </style>
     
</head>
<body >
    <form id="form1" runat="server">
    <div>
        <span style="font-size: 14pt"><strong>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Images/logo.jpg" />
            &nbsp; CMS Elektronisches Lehrmittel<br />
            <br />
        </strong></span>
        Artikelsuche:
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Suche" /><br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="GridView1_RowDataBound" Width="100%" AllowPaging="True" AllowSorting="True" EnableViewState="False" PageSize="20">
            <Columns>
                <asp:BoundField DataField="ART_ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                    SortExpression="ART_ID" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Product" HeaderText="Produkt" SortExpression="Product" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle Width="200px" Font-Underline="True" ForeColor="#EE3E41" />
                </asp:BoundField>
                <asp:BoundField DataField="Kurz" HeaderText="Kurz" SortExpression="Kurz" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Weight" HeaderText="Weight" SortExpression="Weight" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Holz" HeaderText="Holz" ReadOnly="True" SortExpression="Holz" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Glas" HeaderText="Glas" ReadOnly="True" SortExpression="Glas" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Metall" HeaderText="Metall" ReadOnly="True" SortExpression="Metall" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="hasConfig" HeaderText="zugeordnete Themen" SortExpression="hasConfig" >
                    <ItemStyle HorizontalAlign="Center" />
                    <HeaderStyle Wrap="False" />
                </asp:BoundField>
                <asp:TemplateField>
                 <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/App_Images/edit.gif" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"/>
            <RowStyle BackColor="#DEDEDE" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        &nbsp; &nbsp;&nbsp;&nbsp;
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="EDU_getProductList" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtSearch" Name="SearchText" PropertyName="Text"
                    Type="String" DefaultValue="@" />
            </SelectParameters>
        </asp:SqlDataSource>
    
    </div>
        
       
    </form>
</body>
</html>
