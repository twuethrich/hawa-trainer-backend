﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AttributeList" Codebehind="AttributeList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Unbenannte Seite</title>
    <script src="JScript.js" type="text/Jscript"></script>
     <style type="text/css">
        body {font-family: Arial; font-size:8pt;}
        textarea{font-family: Arial;font-size:8pt;}
        td{vertical-align:top;}
        input{font-size:8pt;cursor:hand;}
        select{font-size:8pt;}
    </style>
    <script type="text/javascript">
    function setATTID(attID){
        document.forms[0].hdfattID.value = attID;
    }
    </script> 
</head>
   
<body>
    <form id="form1" runat="server">
    <div>
        <input id="hdfattID" runat="server" type="hidden" />
        
       
       
            <table >
                <tr>
                    <td style="width: 300px;font-size: 12pt;font-weight: bold">
                        Attribute</td>
                    <td align="right" rowspan="2" style="width: 180px">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Images/logo.jpg" /></td>
                </tr>
                <tr>
                    <td style="width: 300px;">
                        <span style="font-family: Webdings">8</span>
                        <asp:Label ID="Label1" runat="server" Font-Size="10pt" Text="Label"></asp:Label>&nbsp;<br />
                        <span style="font-family: Webdings">8</span>
                        <asp:Label ID="Label2" runat="server" Font-Size="10pt" Text="Label" ></asp:Label>
                        </td>
                </tr>
            </table>
       
        <br />
        &nbsp;<asp:Button ID="Button1" runat="server" Text="Neuer Eintrag" />
        <br />
        <br />
        <div style="overflow:auto; width:490px;height:490px">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="ATT_ID" Width="95%" EnableViewState="False">
            
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            
            <Columns>
                <asp:BoundField DataField="ATT_ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                    SortExpression="ATT_ID" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle Width="5px" />
                </asp:BoundField>
                <asp:BoundField DataField="ATL_NAME" HeaderText="Attribut" SortExpression="ATL_NAME" >
                    <ItemStyle Width="200px" />
                </asp:BoundField>
                <asp:BoundField DataField="ATT_Image1" HeaderText="Bild1" ReadOnly="True" SortExpression="ATT_Image1" Visible="False" />
                <asp:BoundField DataField="ATT_Image2" HeaderText="Bild2" ReadOnly="True" SortExpression="ATT_Image2" Visible="False" />
                <asp:BoundField DataField="ATL_Text1" HeaderText="Text1" ReadOnly="True" SortExpression="ATL_Text1" >
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="ATL_Text2" HeaderText="Text2" ReadOnly="True" SortExpression="ATL_Text2" >
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Sort" SortExpression="Sort">
                      <ItemTemplate>
                          &nbsp;<asp:DropDownList ID="cmbSort" runat="server" OnSelectedIndexChanged="cmbSort_SelectedIndexChanged" AutoPostBack="True" EnableViewState="False" >
                          </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Sort" SortExpression="Sort" ItemStyle-Wrap=false>
                      <ItemTemplate>
                 <asp:LinkButton ID="lnkDE" runat="server">de</asp:LinkButton>
                 <asp:LinkButton ID="lnkEN" runat="server">en</asp:LinkButton>
                 <asp:LinkButton ID="lnkFR" runat="server">fr</asp:LinkButton>
                 <asp:LinkButton ID="lnkES" runat="server">es</asp:LinkButton>
                  </ItemTemplate>
                </asp:TemplateField>
                 
                 <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/App_Images/edit.gif" />
                    </ItemTemplate>
                     <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                   <ItemTemplate>
                       <asp:ImageButton CommandName="Delete" ID="btnDelete" runat="server" ImageUrl="~/App_Images/trash.gif" OnClick="btnDelete_Click" />
                   </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
           
            <RowStyle BackColor="#DEDEDE" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#EE3E41" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#EE3E41" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView> </div>
        &nbsp;</div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CLB_HawaConnectionString %>"
            SelectCommand="EDU_getAttributeList" SelectCommandType="StoredProcedure" DeleteCommand="EDU_DeleteAttribute" DeleteCommandType="StoredProcedure" InsertCommand="EDU_InsertNewAttributeItem" InsertCommandType="StoredProcedure" OnSelected="SqlDataSource1_Selected">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="ART_ID" QueryStringField="artID" Type="Int32" />
                <asp:QueryStringParameter DefaultValue="1" Name="THM_ID" QueryStringField="thmID" Type="Int32" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ATT_ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="ART_ID" QueryStringField="artID" Type="Int32" />
                <asp:QueryStringParameter DefaultValue="1" Name="THM_ID" QueryStringField="thmID" Type="Int32" />
                <asp:Parameter Name="ATL_Name" Type="String" />
                <asp:Parameter Name="ATL_Name2" Type="String" />
                <asp:Parameter Name="ATT_Image1" Type="String" />
                <asp:Parameter Name="ATT_Image2" Type="String" />
                <asp:Parameter Name="ATL_Text1" Type="String" />
                <asp:Parameter Name="ATL_Text2" Type="String" />
                <asp:Parameter Name="AGR_ID" Type="Int32" />
                <asp:Parameter Name="RBT_ID" Type="Int32" />
                <asp:Parameter Direction="InputOutput" Name="ATT_ID" Type="Int32" />
            </InsertParameters>
        </asp:SqlDataSource>
        &nbsp;
    </form>
</body>
</html>
