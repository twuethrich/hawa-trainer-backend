﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;


public partial class CreateThumbNail : System.Web.UI.Page
{

    public string FilePath = ConfigurationManager.AppSettings["ObjectPath"];
    protected void Page_Load(object sender, EventArgs e)
    {
         
            string Image = Request.QueryString["Image"];

            if (Image == null)  

            {
                  this.ErrorResult();
                  return;
            }

            string sSize = Request["Size"]; 
            int Size = 80;
            
            if (sSize != null)
                  Size = Int32.Parse(sSize);
            string Path = Image;
            Bitmap bmp = Thumbnail.CreateThumbnail(Path,Size,Size);
            if (bmp == null) 
            {
                  this.ErrorResult();
                  return;
            }
         

            // Put user code to initialize the page here
            Response.ContentType = "image/jpeg";
            bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            bmp.Dispose();
    }
    private void ErrorResult()
    {
        Response.Clear();
        Response.StatusCode = 404;
        Response.End();
    }
   






}
