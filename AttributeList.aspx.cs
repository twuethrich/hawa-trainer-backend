﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AttributeList : System.Web.UI.Page
{
    public int artID;
    public int thmID;

    protected void Page_Load(object sender, EventArgs e)
    {
       
    
        if (!Page.IsPostBack)
        {

            if (Request.QueryString.Get("thmID") != null){
                Session.Add("thmID",Convert.ToInt32(Request.QueryString.Get("thmID")));
                thmID = Convert.ToInt32(Request.QueryString.Get("thmID"));
            }
            if (Request.QueryString.Get("artID") != null){
                Session.Add("artID",Convert.ToInt32(Request.QueryString.Get("artID")));
                artID = Convert.ToInt32(Request.QueryString.Get("artID"));
            }
            if (Request.QueryString.Get("thema") != null)
                Session.Add("Theme", Request.QueryString.Get("thema"));
        }
         
        
      

       Label2.Text = Session["Theme"].ToString();
       Label1.Text = Session["Product"].ToString();
   
       string xLinkNew = "AttributeDetail.aspx?attID=-1&attribute=Neu&artID="+Session["artID"]+"&thmID="+Session["thmID"];
       Button1.Attributes.Add("onclick", "openWindow('" + xLinkNew + "','newAttribute', 500,680)");

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        thmID = Convert.ToInt32(Session["thmID"]);
        artID = Convert.ToInt32(Session["artID"]);
        if (thmID == 6)
        {
            
            e.Row.Cells[5].Visible = false;
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Text = "Problem";
                e.Row.Cells[4].Text = "Lösung";
            }
        }

       
      

            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
                LinkButton lnkDE = (LinkButton)e.Row.FindControl("lnkDE");
                LinkButton lnkEN = (LinkButton)e.Row.FindControl("lnkEN");
                LinkButton lnkFR = (LinkButton)e.Row.FindControl("lnkFR");
                LinkButton lnkES = (LinkButton)e.Row.FindControl("lnkES");

                int attID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ATT_ID"));
                string attribute = DataBinder.Eval(e.Row.DataItem, "ATL_Name").ToString();
                string xLink = "AttributeDetail.aspx?attID=" + attID + "&artID="+artID+"&thmID="+thmID;
                //Fehlerbeseitigung
                if (thmID == 6)
                {
                    e.Row.Cells[4].Text = DataBinder.Eval(e.Row.DataItem, "ATL_NAME2").ToString();
                    //xLink = "ProblemSolution.aspx?attID=" + attID + "&attribute=" + attribute;
                }

                btnEdit.Attributes.Add("onclick", "openWindow('" + xLink + "','newAttribute', 500,700)");
                btnEdit.Visible = false;
                lnkDE.Attributes.Add("onclick", "openWindow('" + xLink + "&langID=DE','newAttribute', 500,700)");
                lnkEN.Attributes.Add("onclick", "openWindow('" + xLink + "&langID=EN','newAttribute', 500,700)");
                lnkFR.Attributes.Add("onclick", "openWindow('" + xLink + "&langID=FR','newAttribute', 500,700)");
                lnkES.Attributes.Add("onclick", "openWindow('" + xLink + "&langID=ES','newAttribute', 500,700)");
                
                //Combo Sort
                DropDownList cmbSort = (DropDownList)e.Row.FindControl("cmbSort");
                cmbSort.ID = attID.ToString();
                cmbSort.EnableViewState = false;

                int rCount = Convert.ToInt32(Session["rowCount"]);

                for (int i = 0; i < rCount; i++)
                {
                    cmbSort.Items.Add((i+1).ToString());
                    cmbSort.Items[i].Value = (i+1).ToString();
                }
                cmbSort.Attributes.Add("onchange", "setATTID("+ attID+ ")");
                cmbSort.SelectedValue = DataBinder.Eval(e.Row.DataItem, "Sort").ToString();


            }


        
    
            

    }


    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {
        
        
        string scriptString = "<script language=JavaScript> " +
        "window.opener.document.forms[0].submit();self.focus();</script>";


        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                               "script", scriptString);
        }
    }
    protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
       
        Session.Add("rowCount",e.AffectedRows);

    }
    protected void cmbSort_SelectedIndexChanged(object sender, EventArgs e)
    {
        thmID = Convert.ToInt32(Session["thmID"]);
        artID = Convert.ToInt32(Session["artID"]);
        string thema = Session["Theme"].ToString();
        int attID = Convert.ToInt32(this.hdfattID.Value);
        DropDownList cmbSort = sender as DropDownList;
      
        //DropDownList cmbSort = (DropDownList).FindControl("+attID+");
        int sort = Convert.ToInt32(cmbSort.SelectedValue);

        DataAccess.updateAttributeSort(attID, artID, thmID, sort);
        
        Response.Redirect("AttributeList.aspx?thmID=" + thmID + "&artID=" + artID + "&thema=" + thema);
    }
}
