﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Zusammenfassungsbeschreibung für DataAccess
/// </summary>
public class DataAccess
{

    
    public static string strConnString = ConfigurationManager.ConnectionStrings["CLB_HawaConnectionString"].ConnectionString;
    
    
    // speichert die Zuweisung der Themen zu einem System
    public static void updateProductTheme(int artID, int thmID, int del)
    {
        SqlConnection sqlConnection = new SqlConnection(strConnString);
        sqlConnection.Open();
        SqlCommand command = new SqlCommand("EDU_updateProductTheme", sqlConnection);
        command.CommandType = CommandType.StoredProcedure;
        SqlParameter param;
        param = command.Parameters.Add("@ART_ID", SqlDbType.Int);
        param.Direction = ParameterDirection.Input;
        param.Value = artID;
        param = command.Parameters.Add("@THM_ID", SqlDbType.Int);
        param.Value = thmID;
        param = command.Parameters.Add("@del", SqlDbType.Int);
        param.Value = del;
        command.ExecuteNonQuery();
        sqlConnection.Close();
    }
    public static void updateAttribute(int ATT_ID,int ART_ID, int THM_ID,string ATL_Name,string ATL_Name2, string ATT_Image1,string ATT_Image2,string ATL_Text1,string ATL_Text2,int ATT_ImgGroup1,int ATT_ImgGroup2, string LanguageID)
    {
        SqlConnection sqlConnection = new SqlConnection(strConnString);
        sqlConnection.Open();
        SqlCommand command = new SqlCommand("EDU_updateAttribute", sqlConnection);
        command.CommandType = CommandType.StoredProcedure;
        SqlParameter param;
        param = command.Parameters.Add("@ATT_ID", SqlDbType.Int);
        param.Value = ATT_ID;
        param = command.Parameters.Add("@ART_ID", SqlDbType.Int);
        param.Value = ART_ID;
        param = command.Parameters.Add("@THM_ID", SqlDbType.Int);
        param.Value = THM_ID;
        param = command.Parameters.Add("@ATL_Name", SqlDbType.VarChar);
        param.Value = ATL_Name;
        param = command.Parameters.Add("@ATL_Name2", SqlDbType.VarChar);
        param.Value = ATL_Name2;
        param = command.Parameters.Add("@ATT_Image1", SqlDbType.VarChar);
        param.Value = ATT_Image1;
        param = command.Parameters.Add("@ATT_Image2", SqlDbType.VarChar);
        param.Value = ATT_Image2;
        param = command.Parameters.Add("@ATL_Text1", SqlDbType.VarChar);
        param.Value = ATL_Text1;
        param = command.Parameters.Add("@ATL_Text2", SqlDbType.VarChar);
        param.Value = ATL_Text2;
        param = command.Parameters.Add("@ATT_ImgGroup1", SqlDbType.Int);
        param.Value = ATT_ImgGroup1;
        param = command.Parameters.Add("@ATT_ImgGroup2", SqlDbType.Int);
        param.Value = ATT_ImgGroup2;
        param = command.Parameters.Add("@LanguageID", SqlDbType.Char);
        param.Value = LanguageID;
        command.ExecuteNonQuery();
        sqlConnection.Close();
    }
    public static void updateAttributeSort(int ATT_ID, int ART_ID, int THM_ID, int Sort)
    {
        SqlConnection sqlConnection = new SqlConnection(strConnString);
        sqlConnection.Open();
        SqlCommand command = new SqlCommand("EDU_UpdateAttributeSortorder", sqlConnection);
        command.CommandType = CommandType.StoredProcedure;
        SqlParameter param;
        param = command.Parameters.Add("@ATT_ID", SqlDbType.Int);
        param.Value = ATT_ID;
        param = command.Parameters.Add("@ART_ID", SqlDbType.Int);
        param.Value = ART_ID;
        param = command.Parameters.Add("@THM_ID", SqlDbType.Int);
        param.Value = THM_ID;
        param = command.Parameters.Add("@SortNr", SqlDbType.Int);
        param.Value = Sort;
          command.ExecuteNonQuery();
        sqlConnection.Close();
    }
    public static int insertAttribute(int ART_ID,int THM_ID, string ATL_Name, string ATL_Name2,string ATT_Image1, string ATT_Image2, string ATL_Text1, string ATL_Text2, int AGR_ID, int ATT_ImgGroup1, int ATT_ImgGroup2, int RBT_ID)
    {
        SqlConnection sqlConnection = new SqlConnection(strConnString);
        sqlConnection.Open();
        SqlCommand command = new SqlCommand("EDU_InsertNewAttributeItem", sqlConnection);
        command.CommandType = CommandType.StoredProcedure;
        SqlParameter param;
        param = command.Parameters.Add("@ART_ID", SqlDbType.Int);
        param.Value = ART_ID;
        param = command.Parameters.Add("@THM_ID", SqlDbType.Int);
        param.Value = THM_ID;
        param = command.Parameters.Add("@ATL_Name", SqlDbType.VarChar);
        param.Value = ATL_Name;
        param = command.Parameters.Add("@ATL_Name2", SqlDbType.VarChar);
        param.Value = ATL_Name2;
        param = command.Parameters.Add("@ATT_Image1", SqlDbType.VarChar);
        param.Value = ATT_Image1;
        param = command.Parameters.Add("@ATT_Image2", SqlDbType.VarChar);
        param.Value = ATT_Image2;
        param = command.Parameters.Add("@ATL_Text1", SqlDbType.VarChar);
        param.Value = ATL_Text1;
        param = command.Parameters.Add("@ATL_Text2", SqlDbType.VarChar);
        param.Value = ATL_Text2;
        param = command.Parameters.Add("@AGR_ID", SqlDbType.VarChar);
        param.Value = AGR_ID;
        param = command.Parameters.Add("@RBT_ID", SqlDbType.VarChar);
        param.Value = RBT_ID;
        param = command.Parameters.Add("@ATT_ImgGroup1", SqlDbType.Int);
        param.Value = ATT_ImgGroup1;
        param = command.Parameters.Add("@ATT_ImgGroup2", SqlDbType.Int);
        param.Value = ATT_ImgGroup2;
        SqlParameter attID = new SqlParameter("@ATT_ID", SqlDbType.Int);
        attID.Direction = ParameterDirection.Output;
        command.Parameters.Add(attID);

        command.ExecuteNonQuery();
        sqlConnection.Close();
        int ATT_ID = Convert.ToInt32(attID.Value);

        return ATT_ID;

    }

    public static void deleteImage(int ATT_ID, int ImageNr)
    {
        SqlConnection sqlConnection = new SqlConnection(strConnString);
        sqlConnection.Open();
        SqlCommand command = new SqlCommand("EDU_DeleteImage", sqlConnection);
        command.CommandType = CommandType.StoredProcedure;
        SqlParameter param;
        param = command.Parameters.Add("@ATT_ID", SqlDbType.Int);
        param.Value = ATT_ID;
        param = command.Parameters.Add("@ImageNr", SqlDbType.Int);
        param.Value = ImageNr;
        command.ExecuteNonQuery();
        sqlConnection.Close();
    }
}
