﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------



public partial class ProductList {
    
    /// <summary>
    /// form1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlForm form1;
    
    /// <summary>
    /// Image1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Image Image1;
    
    /// <summary>
    /// txtSearch-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtSearch;
    
    /// <summary>
    /// btnSearch-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnSearch;
    
    /// <summary>
    /// GridView1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView GridView1;
    
    /// <summary>
    /// SqlDataSource1-Steuerelement
    /// </summary>
    /// <remarks>
    /// Automatisch generiertes Feld
    /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource1;
}
